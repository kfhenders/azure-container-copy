﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace Kfh.AzCopy
{
    public class IdProvider : IDisposable
    {

        private static object _lockObj = new Object();
        private static IdProvider _current = null;


        private bool _disposed = false;
      
        StreamReader _reader = null;

        public static IdProvider Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_lockObj)
                    {
                        if (_current == null)
                        {
                            _current = new IdProvider();
                        }
                    }
                }
                return _current;
            }
        }

        private IdProvider()
        {
            _reader = new StreamReader(Program.SourceIdsFile);
        }

        public List<string> GetNextIds(int chunkSize)
        {
            List<string> ids = new List<string>();
            lock (_lockObj)
            {
                int i = 0;
                while (i < chunkSize && !_reader.EndOfStream)
                {
                    ids.Add(_reader.ReadLine().Trim());
                    i++;
                }
            }
            return ids;

        }

        #region IDisposable Members

        public void Dispose()
        {
            // No Exceptions from dispose
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch { }
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (_reader != null)
                {
                    _reader.Dispose();
                    _reader = null;
                }
            }
            _disposed = true;
        }

        #endregion

    }
}
