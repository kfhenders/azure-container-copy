﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzCopy
{
    public class BlobContainers
    {
        const string SASPolicyName = "Kfh.AzCopy";

        private static long _missingCount = 0;
        private static long _submittedCount = 0;
        private static long _failedCount = 0;

        private static object _lockObj = new object();
        private static string _sourceSharedAccessSig = null;

        private CloudStorageAccount _sourceAccount;
        private CloudBlobClient _sourceClient;
        private CloudBlobContainer _sourceContainer;
        private CloudStorageAccount _destAccount;
        private CloudBlobClient _destClient;
        private CloudBlobContainer _destContainer;

 
        public BlobContainers( 
            string sourceConnectionString, 
            string sourceContainerName,
            string destConnectionString, 
            string destContainerName)
        {
            _sourceAccount = CloudStorageAccount.Parse(sourceConnectionString);
            _sourceClient = _sourceAccount.CreateCloudBlobClient();
            _sourceContainer = _sourceClient.GetContainerReference(sourceContainerName);

            _destAccount = CloudStorageAccount.Parse(destConnectionString);
            _destClient = _destAccount.CreateCloudBlobClient();
            _destContainer = _destClient.GetContainerReference(destContainerName);
            _destContainer.CreateIfNotExists();

            if (_sourceSharedAccessSig == null)
            {
                lock (_lockObj)
                {
                    if (_sourceSharedAccessSig == null)
                    {
                        SetSourceSharedAccessSig(SASPolicyName);
                    }
                }
            }
        }

        public static long MissingCount
        {
            get { return Interlocked.Read(ref _missingCount); }
        }

        public static long SubmittedCount
        {
            get { return Interlocked.Read(ref _submittedCount); }
        }

        public static long FailedCount
        {
            get { return Interlocked.Read(ref _failedCount); }
        }

        /// <summary>
        /// Copies the blob from source to destination 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> CloudCopy(string id)
        {
            string result = null;
            try
            {
                var sourceBlob = _sourceContainer.GetBlockBlobReference(id);
                var destBlob = _destContainer.GetBlockBlobReference(id);
                Uri sourceSasUri = new Uri(sourceBlob.StorageUri.PrimaryUri + _sourceSharedAccessSig);
                result = await destBlob.StartCopyFromBlobAsync(sourceSasUri).ConfigureAwait(false);
                Interlocked.Increment(ref _submittedCount);
            }
            catch (StorageException ex)
            {
                // Missing
                if (ex.RequestInformation.HttpStatusCode == 404)
                {
                    Interlocked.Increment(ref _missingCount);
                    LogMissingBlob(id);
                }
                else
                {
                    LogCopyFailures(id, ex.Message);
                    LogError(id, ex);
                }
            }
            catch (Exception ex)
            {
                LogCopyFailures(id, ex.Message);
                LogError(id, ex);
            }
            return result;

        }

        /// <summary>
        /// Retries any assets that may have failed. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> RetryCloudCopy(string id)
        {
            // Failures are often caused by Pending copy operations against the destination blob.
            // Check if there are any, and if so Abort it. 
            // Don't bother handling any Storage errors here, let them bubble out from the CloudCopy method
            try
            {
                var blob = await _destContainer.GetBlobReferenceFromServerAsync(id);
                if (blob != null && blob.CopyState != null && blob.CopyState.Status == CopyStatus.Pending)
                {
                    await blob.AbortCopyAsync(blob.CopyState.CopyId);
                    Thread.Sleep(250); // can take a little bit before it catches
                }
            }
            catch { }
            return await CloudCopy(id);
        }

        private void SetSourceSharedAccessSig(string policyName)
        {

            SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();
            policy.SharedAccessExpiryTime = DateTime.UtcNow.AddDays(2);
            policy.Permissions = SharedAccessBlobPermissions.Read;

            BlobContainerPermissions sourceContainerPermissions = new BlobContainerPermissions();
            sourceContainerPermissions.SharedAccessPolicies.Add(policyName, policy);
            _sourceContainer.SetPermissions(sourceContainerPermissions);
            _sourceSharedAccessSig = _sourceContainer.GetSharedAccessSignature(new SharedAccessBlobPolicy(), policyName);
            // Sleep for a sec to make sure it catches
            Thread.Sleep(1000);

        }

        static object _missingBlobLockObj = new object();
        private void LogMissingBlob(string id)
        {
            lock (_missingBlobLockObj)
            {
                using (StreamWriter sw = new StreamWriter(Program.MissingIdsFile, true))
                {
                    sw.WriteLine("{0},{1}", DateTime.Now, id);
                }
            }
        }

        static object _copyFailureLockoObj = new object();
        private void LogCopyFailures(string id, string reason)
        {
            Interlocked.Increment(ref _failedCount);
            lock (_copyFailureLockoObj)
            {
                using (StreamWriter sw = new StreamWriter(Program.FailedIdsFile, true))
                {
                    sw.WriteLine("{0},{1},{2}", DateTime.Now, id, reason);
                }
            }
        }

        static object _errorLockoObj = new object();
        private void LogError(string id, Exception ex)
        {
            lock (_errorLockoObj)
            {
                using (StreamWriter sw = new StreamWriter(Program.ErrorLog, true))
                {
                    sw.WriteLine("{0}:{1},{2},{3}", DateTime.Now, Environment.NewLine, id, ex);
                }
            }
        }
    }
}
