﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Kfh.AzCopy
{
    class Program
    {

        static string sourceCn = ConfigurationManager.AppSettings["SourceBlobConnectionString"];
        static string sourceContainer = ConfigurationManager.AppSettings["SourceBlobContainerName"];
        static string destCn = ConfigurationManager.AppSettings["DestBlobConnectionString"];
        static string destContainer = ConfigurationManager.AppSettings["DestBlobContainerName"];

        static string _programDataDir = ConfigurationManager.AppSettings["ProgramDataDir"];
        public static string SourceIdsFile = Path.Combine(_programDataDir, "SourceIds.txt");
        public static string LogFile = Path.Combine(_programDataDir, "Log.txt");
        public static string MissingIdsFile = Path.Combine(_programDataDir, "MissingIds.txt");
        public static string FailedIdsFile = Path.Combine(_programDataDir, "FailedIds.txt");
        public static string FailedIdsBeforeRetryFile = Path.Combine(_programDataDir, "FailedIds.BeforeRetry.txt");
        public static string ErrorLog = Path.Combine(_programDataDir, "ErrorLog.txt");

        private static int _chunkSize = int.Parse(ConfigurationManager.AppSettings["ChunkSize"]);    
        static int _logIntervalMs = int.Parse(ConfigurationManager.AppSettings["LogInterValSeconds"]) * 1000;
        static bool _finished = false;

        static void Main(string[] args)
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.DefaultConnectionLimit = 1000;

            if (args.Length != 1 || (args[0] != "copy" && args[0] != "retry"))
            {
                Console.WriteLine("Usage: Kfh.AzCopy copy|retry. Specify only one.");
                Console.ReadLine();
                return;
            }

            switch (args[0])
            {
                case "copy":
                    CopyBlobs();
                    break;
                case "retry":
                    RetryFailures();
                    break;
            }

            Console.WriteLine("Done!");
            Console.ReadLine();
            
        }

        static void CopyBlobs()
        {
            if (!File.Exists(SourceIdsFile))
            {
                Console.WriteLine("Unable to locate file {0}", SourceIdsFile);
                return;
            }

            List<string> filesToDelete = new List<string> { LogFile, MissingIdsFile, FailedIdsFile, ErrorLog };
            foreach (string file in filesToDelete)
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }

            Task.Run(() => LogForSanity(_logIntervalMs, DateTime.Now));
            
            BlobContainers containers = new BlobContainers(sourceCn, sourceContainer, destCn, destContainer);

            List<string> ids = IdProvider.Current.GetNextIds(_chunkSize);
            while (ids.Count > 0)
            {
                Task<string>[] tasks = new Task<string>[ids.Count];
                int index = 0;
                foreach (string id in ids)
                {
                    int currIndex = index;
                    tasks[currIndex] = Task.Run(async () => await containers.CloudCopy(id).ConfigureAwait(false));
                    index++;
                }
                Task.WaitAll(tasks);
                ids = IdProvider.Current.GetNextIds(_chunkSize);
            }
            _finished = true;
            IdProvider.Current.Dispose();
        }

        static void RetryFailures()
        {
            if (!File.Exists(FailedIdsFile))
            {
                Console.WriteLine("Unable to locate file {0}", FailedIdsFile);               
                return;
            }
            if (File.Exists(FailedIdsBeforeRetryFile))
            {
                Console.WriteLine("Please delete or rename file {0} before continuing.", FailedIdsBeforeRetryFile);
                return;
            }

            File.Move(FailedIdsFile, FailedIdsBeforeRetryFile);
            List<string> ids = new List<string>();

            using (StreamReader sr = new StreamReader(FailedIdsBeforeRetryFile))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] parts = line.Split(new char[] { ',' }, 3);
                    ids.Add(parts[1]);
                }
            }

            BlobContainers containers = new BlobContainers(sourceCn, sourceContainer, destCn, destContainer);

            DateTime startTime = DateTime.Now;
            Task<string>[] tasks = new Task<string>[ids.Count];
            int index = 0;
            foreach (string id in ids)
            {
                int currIndex = index;
                tasks[currIndex] = Task.Run(async () => await containers.RetryCloudCopy(id).ConfigureAwait(false));
                index++;
            }
            Task.WaitAll(tasks);

            Task.Run(() => LogForSanity(100, startTime));
            Thread.Sleep(100);
            _finished = true;

        }

        static void LogForSanity(int sleepMs, DateTime startTime)
        {
            DateTime lastLogTime = startTime;
            long lastSubmittedCount = 0;
            Console.WriteLine("{0} - Starting!", startTime);
            while (!_finished)
            {
                Thread.Sleep(sleepMs);
                long submittedCount = BlobContainers.SubmittedCount;
                long intervalSubmittedCount = submittedCount - lastSubmittedCount;
                lastSubmittedCount = submittedCount;
                long failedCount = BlobContainers.FailedCount;
                long missingCount = BlobContainers.MissingCount;
                TimeSpan intervalElapsed = DateTime.Now - lastLogTime;
                TimeSpan overallElapsed = DateTime.Now - startTime;
                lastLogTime = DateTime.Now;

                string message = string.Format("{0} - Elapsed: {1}; Submitted: {2} {3}/sec (avg. {4}/sec); Failed: {5} ({6}%; Missing: {7} ({8}%)",
                    DateTime.Now, // 0
                    overallElapsed.ToString(@"hh\:mm\:ss"), // 1
                    String.Format("{0:#,##0}", submittedCount), // 2
                    (int)(intervalSubmittedCount / intervalElapsed.TotalSeconds), // 3
                    (int)(submittedCount / overallElapsed.TotalSeconds), // 4
                    String.Format("{0:#,##0}", failedCount), // 5
                    (((double)failedCount / submittedCount) * 100).ToString("F2"), // 6
                    String.Format("{0:#,##0}", missingCount), // 7
                    (((double)missingCount / submittedCount) * 100).ToString("F2") // 8
                    );
                Console.WriteLine(message);
                using (StreamWriter sw = new StreamWriter(LogFile, true))
                {
                    sw.WriteLine(message);
                }
            }

        }

    }
}
